import csv

path = 'C:\\Users\\Alireza\\Desktop\\projects\\Python\\linkedinLearning\\database\\user.csv'

# to confirm login


def confirm_login(data_user, data_pass, user, pswd):
    if data_pass[data_user.index(user)] == pswd:
        return True
    return False

# to sign up


def write_to_csv(data):
    with open(path, mode='a', newline='') as mydb:
        username = data["username"]
        pswd = data["pass"]
        email = data["email"]
        csv_writer = csv.writer(mydb, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow([username, pswd, email])

# to sign in


def login(username, password=None):
    with open(path, newline='') as mydb:
        reader = csv.DictReader(mydb)
        li1, li2 = [], []
        for row in reader:
            li1.append(row['username'])
            li2.append(row['pass'])
        return confirm_login(li1, li2, username, password)
