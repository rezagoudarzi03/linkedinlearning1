import csv

path = 'C:\\Users\\Alireza\\Desktop\\projects\\Python\\linkedinLearning\\database\\user.csv'


def search_account(username):
    with open(path, newline='') as mydb:
        reader = csv.DictReader(mydb)
        for row in reader:
            if row['username'] == username:
                return row
        return None


# print(search_account('ali'))
