from flask import Flask, render_template, request
import csv
from account import login
app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/<string:page_name>')
def html_page(page_name):
    return render_template(page_name)


@app.route('/signin', methods=['POST', 'GET'])
def sign_in():
    if request.method == 'POST':
        data = request.form.to_dict()
        if login(data['username'], data['pass']):
            return 'signed in successfuly'
        else:
            return 'wrong password'
    else:
        return 'Error!!!!!!!!!!!'
