import csv

path = 'C:\\Users\\Alireza\\Desktop\\projects\\Python\\linkedinLearning\\database\\certificate.csv'


def make_certificate(data):
    with open(path, mode='a', newline='') as mydb:
        certi_id = data["certi_id"]
        date_times_create = data["date_times_create"]
        certi_level = data["certi_level"]
        certi_title = data["certi_title"]
        eaxm_id = data["eaxm_id"]
        con_id = data["con_id"]
        acc_id = data["acc_id"]
        csv_writer = csv.writer(mydb, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow([certi_id, date_times_create,
                             certi_level, certi_title, eaxm_id, con_id, acc_id])


def print_certificate():
    with open(path, newline='') as mydb:
        return csv.DictReader(mydb)
